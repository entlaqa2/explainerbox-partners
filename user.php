<?php
require('db.php');
$method = $_SERVER['REQUEST_METHOD'];
$request = explode("/", substr(@$_SERVER['PATH_INFO'], 1));
$method_not = array('status' => 'error', 'error' => 'method not supported');
// check if there is a domain in request
if (!$request[0] == '') {
  // check account information
    $domain = $request[0];
    $sql = "SELECT * FROM account WHERE domain = '$domain'";
    $result = $conn->query($sql);
    if ($result->num_rows > 0) {
    // output data of each row
        $row = $result->fetch_assoc();
        $account_id = $row['id'];
        $account_name = $row['name'];
        $account_maximum = $row['maximum'];
        $end_point = $row['end_point'];
        $auth_key = $row['auth_key'];
         // check the auth key
          if (isset($_SERVER['HTTP_AUTH_KEY'])) {
          if ($row['auth_key'] !== $_SERVER['HTTP_AUTH_KEY']) {
            header("HTTP/1.1 401 Unauthorized");
            exit;
          exit; 
          }
        } else {
          header("HTTP/1.1 401 Unauthorized");
          exit;
        }
    } else {
          // show error
        $last_error = array('status' => 'error', 'error' => 'invalid domain name');
        echo json_encode($last_error);
        exit;
    }
} else {
  // show error
  $last_error = array('status' => 'error', 'error' => 'domain name is required');
  echo json_encode($last_error);
  exit;
}

switch ($method) {
  case 'PUT':
    // method not supported
    echo json_encode($method_not);
    break;
  case 'POST':
    // add new user to the domain name
    // check the maximum number
    $sql = "SELECT * FROM user WHERE account_id = '$account_id'";
    $result = $conn->query($sql);
    $current = $result->num_rows;
    if ($account_maximum <= $current) {
      // show maximum number of users error
      $last_error = array('status' => 'error', 'error' => 'account exceeded the maximum users allowed');
      echo json_encode($last_error);
    } else {
      // make sure there is no duplicated
      if (!isset($_SERVER['HTTP_USERNAME'])) {
        $last_error = array('status' => 'error', 'error' => 'username is required');
        echo json_encode($last_error);
        exit;
      }
      $username = $_SERVER['HTTP_USERNAME'];
      $sql = "SELECT * FROM user WHERE account_id = '$account_id' AND username = '$username'";
      $result = $conn->query($sql);
      if ($result->num_rows > 0) {
        // Show duplicated error
        $last_error = array('status' => 'error', 'error' => 'this username is already taken');
        echo json_encode($last_error);
      } else {
        // add user to database
        $sql = "INSERT INTO user (username, account_id)
                VALUES ('$username', '$account_id')";
                if ($conn->query($sql) === TRUE) {
                    // User Added
                    $output = array('status' => 'ok', 'output' => 'user successfully added');
                    echo json_encode($output);
                } else {
                    // Cannot Add
                  $last_error = array('status' => 'error', 'error' => 'Unable to add user into the database');
                  echo json_encode($last_error);
                }
      }
    }
    break;
  case 'GET':
    // check if we have username
    if (isset($request[1])) {
      // validate the licence in case of individual user
      $username = $request[1];
      $sql = "SELECT * FROM user WHERE account_id = '$account_id' AND username = '$username'";
      $result = $conn->query($sql);
       if ($result->num_rows > 0) {
        // valid user licence
        $output = array('status' => 'ok', 'licence' => 'true', 'connection' => $end_point);
        echo json_encode($output);
       } else {
        // invalid user licence
        $output = array('status' => 'ok', 'licence' => 'false');
        echo json_encode($output);
       }

    } else {
      // show all users if we don't have username
      $sql = "SELECT * FROM user WHERE account_id = '$account_id'";
      $result = $conn->query($sql);
          while($r = mysqli_fetch_assoc($result)) {
            $rows[] = $r;
          }
          echo json_encode($rows);
          exit;
    }
    break;
  case 'HEAD':
    // method not supported  
    echo json_encode($method_not);
    break;
  case 'DELETE':
    // make sure we have a valid username
      if (!isset($_SERVER['HTTP_USERNAME'])) {
        $last_error = array('status' => 'error', 'error' => 'username is required');
        echo json_encode($last_error);
        exit;
      }
      $username = $_SERVER['HTTP_USERNAME'];
      $sql = "SELECT * FROM user WHERE account_id = '$account_id' AND username = '$username'";
      $result = $conn->query($sql);
      if ($result->num_rows > 0) {
        // delete user
        $sql = "DELETE FROM user WHERE account_id = '$account_id' AND username = '$username'";

          if ($conn->query($sql) === TRUE) {
              // Confirmation
              $output = array('status' => 'ok', 'output' => 'user successfully deleted');
                    echo json_encode($output);
          } else {
              // Unable to delete
            $last_error = array('status' => 'error', 'error' => 'unable to delete this user');
          echo json_encode($last_error);
          }

      } else {
        // return error that user is not registered
        $last_error = array('status' => 'error', 'error' => 'the username you are trying to delete is not registered');
        echo json_encode($last_error);
        exit;
      }
    
    break;
  case 'OPTIONS':
    //method not supported
    echo json_encode($method_not);
    break;
  default:
    echo json_encode($method_not);
    break;
}
?>