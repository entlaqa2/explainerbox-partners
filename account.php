<?php
require('db.php');
$method = $_SERVER['REQUEST_METHOD'];
$request = explode("/", substr(@$_SERVER['PATH_INFO'], 1));
$method_not = array('status' => 'error', 'error' => 'method not supported');

switch ($method) {
  case 'PUT':
    // method not supported
    echo json_encode($method_not);
    break;
  case 'POST':
    // method not supported
    echo json_encode($method_not);
    break;
  case 'GET':
    // Searching for account information on database
    $domain = $request[0];
    $sql = "SELECT * FROM account WHERE domain = '$domain'";
    $result = $conn->query($sql);
    if ($result->num_rows > 0) {
    // output data of each row
        $row = $result->fetch_assoc();
        $account_id = $row['id'];
        $name = $row['name'];
        $maximum = $row['maximum'];
        $auth_key = $row['auth_key'];
        $company_id = $row['company_id'];

            // Check Auth Key
            if (isset($_SERVER['HTTP_AUTH_KEY'])) {
                $sql = "SELECT * FROM partner WHERE id = '$company_id'";
                $result = $conn->query($sql);
                $row = $result->fetch_assoc();
            // Check Company Auth Key
            if ($row['auth_key'] == $_SERVER['HTTP_AUTH_KEY']) {
              // Get Current Number of Users
                  $sql = "SELECT id FROM user WHERE account_id = '$account_id'";
                  $result = $conn->query($sql);
                  $current = $result->num_rows;
                    // Present JSON Data
                    $output = array('status' => 'ok', 'name' => $name, 'maximum' => $maximum, 'usage' => $current, 'auth_key' => $auth_key);
                    echo json_encode($output); 

            } else {
              $last_error = array('status' => 'error', 'error' => 'invalid Auth Key');
              header("HTTP/1.1 401 Unauthorized");
              exit;
            } else {
              header("HTTP/1.1 401 Unauthorized");
              exit;
            }

        } else {
            $last_error = array('status' => 'error', 'error' => 'invalid domain name');
            echo json_encode($last_error);
        }
    break;
  case 'HEAD':
    // method not supported  
    echo json_encode($method_not);
    break;
  case 'DELETE':
    //method not supported
    echo json_encode($method_not);
    break;
  case 'OPTIONS':
    //method not supported
    echo json_encode($method_not);
    break;
  default:
    echo json_encode($method_not);
    break;
}
?>